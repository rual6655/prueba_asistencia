package com.example.pruebaasistencia.usecase;

public class Response<T> {
    private T data;
    private boolean error;

    public Response(T data) {
        this.data = data;
        this.error = false;
    }

    public Response() {
        this.error = true;
    }

    public T getData() {
        return data;
    }

    public boolean isError() {
        return error;
    }
}
