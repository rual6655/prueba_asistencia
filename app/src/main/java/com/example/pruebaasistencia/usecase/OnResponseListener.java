package com.example.pruebaasistencia.usecase;

public interface OnResponseListener<T> {
    public void onResponse(Response<T> response);
}
