package com.example.pruebaasistencia.model;

public class Asist {
    private String cod_asistance;
    private String color;
    private String message;
    private String recommendation;

    public String getCod_asistance() {
        return cod_asistance;
    }

    public void setCod_asistance(String cod_asistance) {
        this.cod_asistance = cod_asistance;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }
}
