package com.example.pruebaasistencia.model;

public class AsistResponse {
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Asist getData() {
        return data;
    }

    public void setData(Asist data) {
        this.data = data;
    }

    private String status;
    private Asist data;
}
