package com.example.pruebaasistencia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pruebaasistencia.client.AsistGateway;
import com.example.pruebaasistencia.model.Asist;
import com.example.pruebaasistencia.usecase.OnResponseListener;
import com.example.pruebaasistencia.usecase.Response;

import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

    TextView codasist,message,recommanation;
    LinearLayout lyResp;
    Animation animation_button;
    Button sosButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sosButton = (Button) findViewById(R.id.roll_button);
        lyResp = (LinearLayout) findViewById(R.id.lyResp);

        codasist = (TextView) findViewById(R.id.codasist);
        message = (TextView) findViewById(R.id.message);
        recommanation = (TextView) findViewById(R.id.recommanation);

        animation_button = AnimationUtils.loadAnimation( MainActivity.this,R.anim.zoomout);

        sosButton.startAnimation(animation_button); //inicia la animación

        sosButton.setOnClickListener(actionButton);


    }

    private View.OnClickListener actionButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            actionDelayed();
        }
    };
    private void actionDelayed(){
        final Handler handler= new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                AsistGateway.getGateway().getAsist(new OnResponseListener<Asist>() {
                    @Override
                    public void onResponse(Response<Asist> response) {

                        if (response.isError()){
                            Toast.makeText(MainActivity.this, R.string.api_error, Toast.LENGTH_SHORT).show();
                        }else {

                            sosButton.clearAnimation();
                            Animation animation_response = AnimationUtils.loadAnimation(MainActivity.this, R.anim.sample_anim);
                            lyResp.setVisibility(View.VISIBLE);
                            lyResp.startAnimation(animation_response);
                            codasist.setText(getString(R.string.asist, response.getData().getCod_asistance()));
                            message.setText(response.getData().getMessage());
                            recommanation.setText(getString(R.string.recommanation, response.getData().getRecommendation()));
                            //Log.d("este es el color", ""+ response.getData().getColor());
                            lyResp.getBackground().setColorFilter(Color.parseColor(response.getData().getColor()), PorterDuff.Mode.SRC_ATOP);
                        }
                    }
                });

                handler.postDelayed(this,15000);//se ejecutara cada 15 segundos
            }
        },100);//empezara a ejecutarse después de 1 milisegundo
    }
}
