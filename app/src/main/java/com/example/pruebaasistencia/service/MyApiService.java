package com.example.pruebaasistencia.service;



import com.example.pruebaasistencia.model.Asist;
import com.example.pruebaasistencia.model.AsistResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyApiService {
    @GET("simula_asistencia")
    Call<AsistResponse> getAsist();
}
