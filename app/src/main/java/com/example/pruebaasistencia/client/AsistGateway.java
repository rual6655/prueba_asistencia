package com.example.pruebaasistencia.client;

import android.util.Log;

import com.example.pruebaasistencia.model.Asist;
import com.example.pruebaasistencia.model.AsistResponse;
import com.example.pruebaasistencia.service.MyApiService;
import com.example.pruebaasistencia.usecase.OnResponseListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AsistGateway {
    private final MyApiService apiService;
    private static AsistGateway sInstance;

    public AsistGateway(MyApiService apiService) {
        this.apiService=apiService;
    }

    public static AsistGateway getGateway() {
        if (sInstance == null){
            sInstance = new AsistGateway(MyApiProvider.getApiService());
        }
        return sInstance;
    }

    public void getAsist(final OnResponseListener<Asist> listener){
        Call<AsistResponse> call = MyApiProvider.getApiService().getAsist();
        call.enqueue(new Callback<AsistResponse>() {
            @Override
            public void onResponse(Call<AsistResponse> call, Response<AsistResponse> response) {
                if (listener != null) {

                    com.example.pruebaasistencia.usecase.Response<Asist> resp;
                    if (response.isSuccessful()) {
                        if (!response.body().getStatus().equals("ok")){
                            resp = new com.example.pruebaasistencia.usecase.Response<>();
                        }else{
                            resp = new com.example.pruebaasistencia.usecase.Response<>(response.body().getData());
                        }

                        Log.d("onResponse asistencia", "asistencia =>" + response.body());

                    }else{
                        resp = new com.example.pruebaasistencia.usecase.Response<>();
                    }

                    listener.onResponse(resp);

                }
            }

            @Override
            public void onFailure(Call<AsistResponse> call, Throwable t) {
                if (listener != null) {

                    com.example.pruebaasistencia.usecase.Response<Asist> resp;
                    resp = new com.example.pruebaasistencia.usecase.Response<>();
                    listener.onResponse(resp);
                }
                Log.e("onFailure asistencia", "Failure =>" + call);
            }
        });
    }

}
